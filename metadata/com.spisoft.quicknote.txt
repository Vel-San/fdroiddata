Categories:Writing
License:GPL-3.0-only
Web Site:https://qn.phie.ovh
Source Code:https://github.com/PhieF/CarnetDocumentation
Issue Tracker:https://github.com/PhieF/CarnetAndroid/issues
LiberapayID:34946

Summary:Powerful note taking app with sync capabilities
Description:
Carnet is a powerful note taking app which purpose is not only to write your
shopping list but can also be used to write longer texts, stories, etc.

Carnet is available on Android and Linux, soon on your web browser, Windows and
Mac, with sync capabilities

'''Features'''

* Complete editor : bold/italic/underline/color/highlight
* Import from Google Keep (only on desktop, then sync on mobile)
* Insert images / review
* Open HTML format
* Organize with folders and choose root storage folder depending on your needs
* Keywords
* Quickly see your recent notes
* Search amount your notes
* Protect the app with a pin code (won't encrypt notes) on android
* Statistics : words/sentences/characters
* Sync with NextCloud

'''To come'''

* Audio recording
* NextCloud Online integration (as a nextcloud app)
* Perf improvements
* Lighter version for linux without Electron
* Windows / Mac OS electron app
* Many things I don't think about right now
.

Repo Type:git
Repo:https://github.com/PhieF/CarnetFdroid.git

Build:0.1,1
    commit=e6639d21a1908c030329032d5de25e79afd5043e
    subdir=CarnetAndroid
    submodules=yes
    gradle=fdroid
    output=app/build/outputs/apk/fdroid/release/app-fdroid-release-unsigned.apk
    prebuild=sed -i -e '1d' -e "2s/^/include\ \'\:app\'\,\ \'\:Sync\' \n/" -e '/GoogleSync/d' settings.gradle && \
        sed -i -e '/googleCompile/d' app/build.gradle

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.1
Current Version Code:1
